from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path('infosysnet/', include('index.urls', namespace="index")),
    path('admin/', admin.site.urls),
]
