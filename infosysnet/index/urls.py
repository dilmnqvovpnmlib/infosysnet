from django.urls import path

from . import views

app_name = 'index'
urlpatterns = [
    path('', views.TopView.as_view(), name='top'),
    path('index/', views.IndexView.as_view(), name='index'),
    path('index/remark1/', views.Remark1View.as_view(), name='remark1'),
    path('index/remark2/', views.Remark2View.as_view(), name='remark2'),
    path('index/remark3/', views.Remark3View.as_view(), name='remark3'),
    path('forbidden/', views.ForbiddenClass.as_view(), name='forbidden'),
    path('index/flag/', views.FlakeFlagClass.as_view(), name='fakeflag'),
    path('index/index/', views.FlagClass.as_view(), name='flag'),
]
